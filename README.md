# Colour Picker Game

## Description of project
We made a game using the languages: HTML, CSS and Javascript. The goal of the game is to simply match the colour.

## How to play

1. Open the index.html or go to our site.
2. Fill in the player name (at least 5 alphanumeric characters/spaces).
3. Choose a board size
4. Choose the colour you wish to match
5. Choose the difficulty of the game
6. Press Start
7. Click on the tiles that match the colour you chose
8. Press Guess to get your score

## High Score

- Only scores that have a percentage completion of 50% or above can enter the leaderboard
- When at least 1 score appears in the leaderboard, the clear button will show up
- The clear button is used to clear the leaderboard
- You can press the "High Score" title to change the order of the leaderboard (descending or ascending)
- The leaderboard will only show the top 10 scores

## Features

- You can press the keys Shift + C to show the colour of each tiles

## Credits

- Made by Rida Chaarani and Alexander Roxas
- Some features were inspired by Uyen Dinh Michelle Banh and the getScore() is authored by Samuel Imberman