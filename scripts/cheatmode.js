'use strict';
/**
 * @author Alexander Roxas & Rida Charaani
 *
 * cheatcode.js handles the cheats for the game.
 * Pressing Shift+C gives the string rgb value of the tiles and the dominant color.
 */

/**
 * @description Toggles the visibility of the cheat code when pressing Shift+ C
 */
document.addEventListener("keydown", function(event) {
    
    if (event.shiftKey && event.key === "C") {
        Array.from(document.getElementsByClassName("cheatcode"))
        .forEach((td) => {
            if (td.style.visibility === "visible"){
                td.style.visibility = "hidden";
            } else {
                td.style.visibility = "visible";
            }
            });
    };

});

/**
 * @description Determines the largest colour value out of the Red Green Blue
 * 
 * @param {number} color1 - The Red colour number
 * @param {number} color2 - The Green colour number
 * @param {number} color3 - The Blue colour number
 * @returns {string} The name of the colour which has the largest value ex.Red
 */
function calculateBigger(red, green, blue, userColor){
    if (red > green && red > blue){
        return "red";
    } else if (green > red && green > blue) {
        return "green";
    } else if (blue > green && blue > red) {
        return "blue";
    }
      
    if (red === green && (userColor === "red" || userColor === "green") ){
        return userColor;
    } else if (green === blue && (userColor === "green" || userColor === "blue") ){
        return userColor;
    } else if (red === blue && (userColor === "red" || userColor === "blue")){
        return userColor;
    }

    if (red === blue || red === green){
        return "red";
    } else if (green === blue) {
        return "green";
    }
    if (red === green && green === blue){
        return userColor;
    }

};
