'use strict';
/**
 * @author Alexander Roxas & Rida Charaani
 * 
 * setup.js handles the initial setup of the page.
 * 
 * It has the validation of the inputs.
 * It manages the initial start of the game.
 * It makes the gameboard and everything that is necessary for the board.
 * Disables the inputs when the game starts.
 */

document.addEventListener("DOMContentLoaded", function(){

    const startButton = document.getElementById("start");

    document.querySelector("#colour").addEventListener("input", buttonColorChange);

    startButton.addEventListener("click", function(){
        const name = document.getElementById("name");
        let user = name.value;
        const colorInput = document.getElementById("colour");
        let colorSelect = colorInput.value;
        if(!isValidPlayerName(user)) {
            name.setCustomValidity("Player name must at least have 5 characters and consist of alphanumeric characters or spaces");
            name.reportValidity();
            return;
        } else {
            name.setCustomValidity("");
        }
        
        if(colorSelect === "") {
            colorInput.setCustomValidity("Select a colour before starting");
            colorInput.reportValidity();
            return;
        } else {
            colorInput.setCustomValidity("");
        }
        
        document.getElementById("size").disabled = true;
        document.getElementById("name").disabled = true;
        document.getElementById("difficulty").disabled = true;
        document.getElementById("colour").disabled = true;
        document.getElementById("start").disabled = true;
        document.getElementById("guess").disabled = false;
        
        
        let size = document.getElementById("size").value;
        let difficulty = document.getElementById("difficulty").value;
        tableCreate(size, difficulty);
        tileColor(difficulty, colorSelect);
        const amount = document.getElementById("colorAmount");
        let target = getTarget();
        amount.textContent = `Searching for ${colorSelect} tiles! Your target is ${target} tiles! You have 0 selected!`;
        document.getElementById("guess").disabled = false;

        addEvent();
    });

    
});

/**
 * @description Checks if the player name is valid
 * 
 * @param {string} name - Player name thats going to be validated
 * @returns {boolean} - Gives true if its valid and false if its not valid
 */
function isValidPlayerName(name) {
    return /[a-zA-Z0-9\s]{5,}/.test(name)
}

/**
 * @description Changes the colour of the start button
 */
function buttonColorChange() {
    const startButton = document.getElementById("start");
    const value = document.querySelector("#colour").value;
    startButton.style.backgroundColor = value;
    startButton.style.color = "white";
}
