'use strict';
/**
 * @author Alexander Roxas & Rida Charaani
 *
 * board.js handles all the gameboard logic.
 * 
 * It takes in account all of the form inputs to create the boardgame.
 * It adds an event listener for each tile to be able to be selected.
 * It calculates the color values and changes the colors of each tiles.
 */

/**
 * @description - Creates the game board table with specified size 
 * 
 * @param {number} size - The size of the game board
 * 
 */
function tableCreate (size){
    const board = document.getElementById("gameBoard");
    const table = document.createElement("table");
    const tableBody = document.createElement("tbody");
    board.textContent = undefined;
    board.appendChild(table);
    table.appendChild(tableBody);
    for (let i = 0; i < size; ++i){
        const newRow = document.createElement("tr");
        tableBody.appendChild(newRow);
        
        for (let j = 0; j < size; ++j){
            const newCell = document.createElement("td");
            newRow.appendChild(newCell)
            newCell.classList.add("tile");
        }
    }
    
};

/**
 * @description Adds an event listener to each tiles
 */
function addEvent(){
    const cells = document.querySelectorAll(".tile");

    Array.from(cells).forEach((td) => {
        td.addEventListener("click", function (){
            if (td.classList.contains("selected")){
                td.classList.remove("selected");
            } else {
                td.classList.add("selected");
            }

            let counter = getNumSelected();
            let amount = document.getElementById("colorAmount");
            let colorInput = document.getElementById("colour");
            let colorSelect = colorInput.value;
            amount.textContent = `Searching for ${colorSelect} tiles! Your target is ${document.querySelectorAll(`.${colorSelect}`).length} tiles! You have ${counter} selected!`;
        });
    });
};



/**
 * @description Generates a color for each tile
 * 
 * @param {number} difficulty - Difficulty of the game
 * @param {string} colorSelect - The color the user chose when starting the game
 */
function tileColor(difficulty, colorSelect){
    const cells = document.querySelectorAll(".tile");
    const baseColor = calculateBaseColor(difficulty);

    cells.forEach((td) => {
        let red = calculateRandomColor(difficulty, baseColor);
        let green = calculateRandomColor(difficulty, baseColor);
        let blue = calculateRandomColor(difficulty, baseColor);
        let color = bgChange(red, green, blue);
        td.style.backgroundColor = color;
        let biggestColor = calculateBigger(red, green, blue, colorSelect);
        td.classList.add(`${biggestColor}`);
        
        const newDiv = document.createElement("div");
        td.appendChild(newDiv);
        newDiv.innerHTML = `${color}<br>${biggestColor}`;
        newDiv.classList.add("cheatcode");
        if (red <= 60 && green <= 60 && blue <= 60){
            newDiv.style.color = "white";
        }
    });
};

/**
 * @description Generates a random number within a specified range
 * 
 * @param {number} number - The limit of the random number range
 * @returns {number} - A random number within the range we give
 */
function random(number) {
    return Math.floor(Math.random() * number);
};

/**
 * @description Calculates the base color based on the game difficulty
 * 
 * @param {number} difficulty - The game difficulty level that the user chose
 * @returns {number} - The base colour value
 */
function calculateBaseColor(difficulty){
    if (difficulty == 0){
        return 0;
    }
    if (difficulty == 1){
        return random(255-80);
    }
    if (difficulty == 2){
        return random(255-40);
    }
    if (difficulty == 3){
        return random(255-10);
    }
};

/**
 * @description Calculates a random color based on the game difficulty and base color
 * 
 * @param {number} difficulty - The game difficulty level
 * @param {number} baseColor - The base colour value
 * @returns {number} A random colour value
 */
function calculateRandomColor(difficulty, baseColor) {
    let holder;
    if (difficulty == 0){ 
        holder = random(255);
    }
    if (difficulty == 1){
        holder = baseColor + random(80);
    }
    if (difficulty == 2){
        holder = baseColor + random(40);
    }
    if (difficulty == 3){
        holder = baseColor + random(10); 
    }
    return holder;
}

/**
 * @description Generates an RGB color string based on red, green, and blue values
 * 
 * @param {number} red - The red colour number
 * @param {number} green - The green colour number
 * @param {number} blue - The blue colour number
 * @returns {string} An RGB color string
 */

function bgChange(red, green, blue) {
    return `rgb(${red}, ${green}, ${blue})`;
};


