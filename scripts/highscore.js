'use strict';
/**
 * @author Alexander Roxas & Rida Charaani
 *
 * highscore.js handles all of the highscore logic and related info.
 * 
 * Clicking the "High Score" header makes the order switch to ascending/descending based off the score of each player.
 * The scores are stored in the local storage and is regenerated whenever the page is loaded again.
 */

document.addEventListener("DOMContentLoaded", function(){
    setScore();
    let leaderboardOrder = false;

    document.getElementById("guess").addEventListener("click", function(){
        const boardSize = document.querySelector("#size").value;
        const difficulty = document.querySelector("#difficulty").value;
        const playerName = document.getElementById("name").value;

        let numCorrect = getNumCorrect();
        let numSelected = getNumSelected();
        let percentage = calculatePercentage();
        const span = document.getElementById("colorAmount")

        const score = getScore(numCorrect, numSelected, boardSize, difficulty);
      
        if(percentage >= 50) {
            span.textContent = `Congratulations you got ${percentage}%!! You passed!!`
            addScore(playerName, score);

            setScore();
        } else {
            span.textContent = `You sadly got only ${percentage}%. Please try again to make it to leaderboard!`
        }
        
        resetGame();

        const numScores = document.querySelectorAll("#highScoreBody tr");
        if(numScores.length === 0) {
            document.getElementById("clear").style.visibility = "hidden";
        } else {
            document.getElementById("clear").style.visibility = "visible";
        }
    });


    document.querySelector("h3").addEventListener("click", function(e) {
        if(leaderboardOrder) {
            descending();
            setScore();
        } else {
            ascending();
            setScore();
        }
     });

    /**
     * @description Makes the leaderboard go in ascending order
     */
    function ascending() {
        const leaderboardBody = document.getElementById("highScoreBody");
        leaderboardBody.textContent = undefined;

        let highscore = [];

        if(localStorage.getItem("highscore")) {
            highscore = JSON.parse(localStorage.getItem("highscore"));
        } else {
            return;
        }

        const ascendingArr = highscore.toSorted( ( a ,b ) => a.points - b.points);
        localStorage.setItem("highscore", JSON.stringify(ascendingArr));
        leaderboardOrder = true;
    }

    /**
     * @description Makes the leaderboard go in descneding order 
     */
    function descending() {
        const leaderboardBody = document.getElementById("highScoreBody");
        leaderboardBody.textContent = undefined;

        let highscore = [];

        if(localStorage.getItem("highscore")) {
            highscore = JSON.parse(localStorage.getItem("highscore"));
        } else {
            return;
        }

        const descendingArr = highscore.toSorted( ( a ,b ) => b.points - a.points);
        localStorage.setItem("highscore", JSON.stringify(descendingArr));
        leaderboardOrder = false;
    }

    /**
     * @description Clears the highscore and makes the clear button hidden
     */
    const clearHighscore = () => {
        localStorage.clear();
        const highscoreBody = document.getElementById("highScoreBody");
        while (highscoreBody.firstChild) {
            highscoreBody.removeChild(highscoreBody.firstChild);
        }

        document.getElementById("clear").style.visibility = "hidden";
    }

    document.getElementById("clear").addEventListener("click", clearHighscore);

});

/**
 * @description Calculates the score the user gets
 * 
 * @param {number} numCorrect - Number of corrected tiles
 * @param {number} numSelected - Number of selected tiles
 * @param {number} boardSize - The size of the board
 * @param {number} difficulty - The difficulty of the game
 * @returns 
 */
function getScore(numCorrect, numSelected, boardSize, difficulty) {
    const percent = ( 2 * numCorrect - numSelected )/ (boardSize * boardSize);
    return Math.floor(percent * 100 * boardSize * (difficulty + 1));
};

/**
* @description Resets the game to its original form
*/
function resetGame(){
    const size = document.getElementById("size");
    size.disabled = false;
    size.value = 3;

    const name = document.getElementById("name");
    name.disabled = false;
    name.value = "";

    const colour = document.getElementById("colour");
    colour.disabled = false;
    colour.value = "";

    const difficulty = document.getElementById("difficulty");
    difficulty.disabled = false;
    difficulty.value = 0;

    document.getElementById("guess").disabled = true;
    
    const startButton = document.getElementById("start");
    startButton.disabled = false;
    startButton.style.backgroundColor = "white";
    startButton.style.color = "black";

    document.getElementById("gameBoard").textContent = undefined;

};
    
/**
 * @description Adds the score and the name in local storage
 * @param {string} name - The name of the player
 * @param {number} score - The score of the player
 */
function addScore(name, score) {
    let highscores = [];

    if(localStorage.getItem("highscore")){
        highscores = JSON.parse(localStorage.getItem("highscore"));
    }

    let sortHSArr = highscores.toSorted( (a,b) => b.points - a.points);


    if(sortHSArr.length < 10){
        sortHSArr.push({player: name, points: score});
    }

    if(sortHSArr.length === 10){
        sortHSArr.pop();
        sortHSArr.push({player: name, points: score});
    }

    sortHSArr.sort( (a,b) => b.points - a.points);
    
    localStorage.setItem("highscore", JSON.stringify(sortHSArr));
};

/**
 * @description Sets the score the user got in localStorage and puts it in the leaderboard
 */
function setScore() {
    let highscores = [];

    if(localStorage.getItem("highscore")){
        highscores = JSON.parse(localStorage.getItem("highscore"));
    } else {
        return;
    }
    
    const table = document.getElementById("highScoreBody");
    table.textContent = '';

    highscores.forEach((user) => {
        const newRow = document.createElement("tr");
        const playerNames = document.createElement("td");
        playerNames.textContent = user.player;
        newRow.appendChild(playerNames);

        const playerScore = document.createElement("td");
        playerScore.textContent = user.points;
        newRow.appendChild(playerScore);

        table.appendChild(newRow);
    });
};

/**
 * @description Calculates the percentage the user gets when winning/losing a game
 * 
 * @returns {number} - Percentage the user got when completing the game
 */
function calculatePercentage() {
    let percentage;
    let numSelected = getNumCorrect();
    let numCorrect = getNumCorrect();
    let target = getTarget();

    //We chose to deduct by 10% if they choose more than the target
    let minusPercent = (numSelected - numCorrect)*10;

    if(target === numSelected) {
        percentage= parseInt((numCorrect/target)*100);
    } else {
        percentage = parseInt((numCorrect/target)*100)-minusPercent;
    }

    return percentage;
};


/**
 * @description Calculates the number of tiles you selected
 * 
 * @returns {number} - Number of tiles you selected
 */
function getNumSelected() {
    let numSelected = 0;
    Array.from(document.querySelectorAll("td"))
    .forEach( (td) => {
        if(td.classList.contains("selected")) {
            numSelected++;
        }
    })
    return numSelected;
}

/**
 * @description Calculates the number of tiles you got correctly
 * 
 * @returns {number} - The number of tiles you got correct out of all selected
 */
function getNumCorrect() {
    let numCorrect = 0;
    const bigColour = document.querySelector("#colour").value;
    let titles = document.querySelectorAll("td");
    const selected = 
    Array.from(titles)
    .filter( (td) => td.classList.contains("selected"))
    .forEach( (td) => {
        if(td.classList.contains(`${bigColour}`)) {
            numCorrect++;
        }
    }
    );
    return numCorrect;
}

/**
 * Calculates the target tiles to select
 * 
 * @returns {number} - Number of targeted tiles you need to win
 */
function getTarget() {
    const colour = document.getElementById("colour").value;
    let target = 0;
    
    Array.from(document.querySelectorAll("td"))
    .forEach( (td) => {
        if(td.classList.contains(`${colour}`)) {
            target++;
        }
    })
    return target;
}